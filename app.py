
# importing the necessary dependencies
from flask import Flask, render_template, request,jsonify
from flask_cors import CORS,cross_origin
import pickle
import numpy as np
#import pandas as pd
import scipy.stats as s
#import matplotlib.pyplot as plt
import sklearn
from sklearn.datasets import load_boston
#import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
#import statsmodels.formula.api as smf
from sklearn import preprocessing
from sklearn.linear_model  import Ridge,Lasso,RidgeCV, LassoCV, ElasticNet, ElasticNetCV, LinearRegression
from sklearn.preprocessing import StandardScaler 
import pandas as pd





app = Flask(__name__) # initializing a flask app

@app.route('/',methods=['GET'])  # route to display the home page
@cross_origin()
def homePage():
    return render_template("index.html")

@app.route('/predict',methods=['POST']) # route to show the predictions in a web UI
@cross_origin()
def index():
    if request.method == 'POST':
        try:
            #  reading the inputs given by the user
            #rate_marriage	age	yrs_married	children	religious	educ	occupation	occupation_husb
            rate_marriage=float(request.form['rate_marriage'])
            age = float(request.form['age'])
            yrs_married = float(request.form['yrs_married'])
            children = float(request.form['children'])
            religious = float(request.form['religious'])
            educ = float(request.form['educ'])
            occupation = float(request.form['occupation'])
            occupation_husb = float(request.form['occupation_husb'])
        #    TAX = float(request.form['TAX'])
         #   PTRATIO = float(request.form['PTRATIO'])
          #  LSTAT = float(request.form['LSTAT'])

            model = 'model.pickle'
            stand= 'standardscaler.pickle'
            #trans= 'transform.sav'
  #          loaded_stand = pickle.load(open(stand, 'rb'))
            #loaded_trans = pickle.load(open(trans, 'rb'))
            loaded_model = pickle.load(open(model, 'rb')) # loading the model file from the storage
            # predictions using the loaded model file

           # t=transform([[CRIM,ZN,INDUS,CHAS,NOX,RM,AGE,DIS,TAX,PTRATIO,LSTAT]])
            prediction=loaded_model.predict([[rate_marriage,age,yrs_married,children,religious,educ,occupation,occupation_husb]])

            print('prediction is', prediction)
            # showing the prediction results in a UI
            return render_template('results.html',prediction=prediction[0])
        except Exception as e:
            print('The Exception message is: ',e)
            return e
    # return render_template('results.html')
    else:
        return render_template('index.html')



if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8001, debug=True)
   # app.run(debug=False) # running the app